# Repository for benchmarking work with API via gRPC and simple REST.

## System
![system](images/system.png)

## Code
No use of other tools for speed up results. Only standard library and gRPC packages. 

## Results
All values are in seconds.

Number of Runs: 5
Number of requests by run: 100000

GET request via curl
```
curl -X 'GET' \
  'http://localhost:7000/some-api/v1/get-some-data?pdouble=1.1&pfloat=1.1&pint32=-1&pint64=-1&puint32=1&puint64=1&pbool=true&pstring=some' \
  -H 'accept: application/json'
```

| HTTP | gRPC | gRPC-REST-Gateway |
| ---- | ---- | ----------------- |
| 7.41 | 9.97 |       38.93       |
| 7.35 | 9.98 |       38.84       |
| 7.32 | 9.97 |       40.28       |
| 7.31 | 9.89 |       39.54       |
| 7.31 | 9.95 |       40.29       |

POST request via curl
```
curl -X 'POST' \
  'http://localhost:7000/some-api/v1/get-some-data' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "pdouble": 1.1,
  "pfloat": 1.1,
  "pint32": -1,
  "pint64": "-1",
  "puint32": 1,
  "puint64": "1",
  "pbool": true,
  "pstring": "some"
}'
```

| HTTP |  gRPC | gRPC-REST-Gateway |
| ---- | ----- | ----------------- |
| 8.61 | 10.04 |       40.81       |
| 8.59 | 10.01 |       40.89       |
| 8.59 | 10.03 |       41.01       |
| 8.57 | 10.02 |       40.76       |
| 8.56 | 10.03 |       42.33       |