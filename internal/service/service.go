package service

import (
	"context"

	pb "gitlab.com/zigalo/grpc-rest-test/pkg/grpc_rest_test_service"
	"google.golang.org/grpc"
)

type Service struct {
	pb.UnimplementedGrpcRestTestServiceServer
}

func New() *Service {
	return &Service{}
}

func (s *Service) GetData(ctx context.Context, in *pb.GetDataRequest) (*pb.GetDataResponse, error) {
	return &pb.GetDataResponse{}, nil
}

func (s *Service) PostData(ctx context.Context, in *pb.PostDataRequest) (*pb.PostDataResponse, error) {
	return &pb.PostDataResponse{}, nil
}

func (s *Service) RegisterGRPC(server *grpc.Server) {
	pb.RegisterGrpcRestTestServiceServer(server, s)
}
