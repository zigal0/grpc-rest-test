package domain

import "fmt"

type Request struct {
	Pdouble float64 `json:"pdouble,float64"`
	Pfloat  float32 `json:"pfloat,float32"`
	Pint32  int32   `json:"pint32,int32"`
	Pint64  int64   `json:"pint64,int64"`
	Puint32 uint32  `json:"puint32,uint32"`
	Puint64 uint64  `json:"puint64,uint64"`
	Pbool   bool    `json:"pbool,bool"`
	Pstring string  `json:"pstring,string"`
}

func (r *Request) ToGetParams() string {
	return fmt.Sprintf(
		"pdouble=%v&pfloat=%v&pint32=%d&pint64=%d&puint32=%d&puint64=%d&pbool=%t&pstring=%s",
		r.Pdouble, r.Pfloat, r.Pint32, r.Pint64, r.Puint32, r.Puint64, r.Pbool, r.Pstring,
	)
}
