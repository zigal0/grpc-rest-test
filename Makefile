include architect.mk
 
run-http-server:
	go run cmd/http-server/main.go

run-grpc-server:
	go run cmd/grpc-server/main.go

run-client:
	go run cmd/client/main.go