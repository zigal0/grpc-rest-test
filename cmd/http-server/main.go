package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/zigalo/grpc-rest-test/internal/domain"
)

func main() {
	http.HandleFunc("/get-data", getHandler)
	http.HandleFunc("/post-data", postHandler)
	http.HandleFunc("/get-dump", getDumpHandler)

	log.Fatal(http.ListenAndServe(":7000", nil))
}

func getHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		panic(err)
	}

	pdouble, err := strconv.ParseFloat(r.Form.Get("pdouble"), 10)
	if err != nil {
		panic(err)
	}

	pfloat, err := strconv.ParseFloat(r.Form.Get("pfloat"), 10)
	if err != nil {
		panic(err)
	}

	pint32, err := strconv.ParseInt(r.Form.Get("pint32"), 10, 32)
	if err != nil {
		panic(err)
	}

	pint64, err := strconv.ParseInt(r.Form.Get("pint64"), 10, 64)
	if err != nil {
		panic(err)
	}

	puint32, err := strconv.ParseUint(r.Form.Get("puint32"), 10, 32)
	if err != nil {
		panic(err)
	}

	puint64, err := strconv.ParseUint(r.Form.Get("puint64"), 10, 64)
	if err != nil {
		panic(err)
	}

	pbool, err := strconv.ParseBool(r.Form.Get("pbool"))
	if err != nil {
		panic(err)
	}

	pstring := r.Form.Get("pstring")

	_ = domain.Request{
		Pdouble: pdouble,
		Pfloat:  float32(pfloat),
		Pint32:  int32(pint32),
		Pint64:  pint64,
		Puint32: uint32(puint32),
		Puint64: puint64,
		Pbool:   pbool,
		Pstring: pstring,
	}

	r.Body.Close()

	w.WriteHeader(200)
	w.Write(nil)
}

func postHandler(w http.ResponseWriter, r *http.Request) {
	data := domain.Request{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err != nil {
		panic(err)
	}

	r.Body.Close()

	w.WriteHeader(200)
	w.Write(nil)
}

func getDumpHandler(w http.ResponseWriter, r *http.Request) {
	r.Body.Close()

	w.WriteHeader(200)
	w.Write(nil)
}

// curl -X GET 'http://localhost:7000/get-data?pdouble=1.1&pfloat=1.1&pint32=-1&pint64=-1&puint32=1&puint64=1&pbool=true&pstring=some'
// curl -X POST -d '{"pdouble":1.1,"pfloat":1.1,"pint32":-1,"pint64":-1,"puint32":1,"puint64":1,"pbool":true,"pstring":"some"}' http://localhost:7000/get-data
