package main

import (
	"context"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.com/zigalo/grpc-rest-test/internal/service"
	pb "gitlab.com/zigalo/grpc-rest-test/pkg/grpc_rest_test_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	listener, err := net.Listen("tcp", ":7002")
	if err != nil {
		log.Fatalln("Failed to listen:", err)
	}

	s := service.New()

	grpcServer := grpc.NewServer()

	s.RegisterGRPC(grpcServer)

	log.Println("Serving gRPC on 0.0.0.0:7002")
	go func() {
		log.Fatalln(grpcServer.Serve(listener))
	}()

	conn, err := grpc.NewClient(
		"0.0.0.0:7002",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		log.Fatalln("Failed to dial server:", err)
	}

	gwmux := runtime.NewServeMux()

	err = pb.RegisterGrpcRestTestServiceHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register gateway:", err)
	}

	gwServer := &http.Server{
		Addr:    ":7000",
		Handler: gwmux,
	}

	log.Println("Serving gRPC-Gateway on http://0.0.0.0:7000")
	log.Fatalln(gwServer.ListenAndServe())
}
