package main

import (
	"net"

	"gitlab.com/zigalo/grpc-rest-test/internal/service"
	"google.golang.org/grpc"
)

func main() {
	s := service.New()

	grpcServer := grpc.NewServer()

	s.RegisterGRPC(grpcServer)

	listener, err := net.Listen("tcp", "localhost:7002")
	if err != nil {
		panic(err)
	}

	if err := grpcServer.Serve(listener); err != nil {
		panic(err)
	}
}
