package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/zigalo/grpc-rest-test/internal/domain"
	pb "gitlab.com/zigalo/grpc-rest-test/pkg/grpc_rest_test_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	numberOfRuns          = 5
	numberOfrequestsByRun = 100000
)

func main() {
	runHTTPTest()
	// rungRPCTest()
}

func runHTTPTest() {
	data := domain.Request{
		Pdouble: 1.1,
		Pfloat:  1.1,
		Pint32:  -1,
		Pint64:  -1,
		Puint32: 1,
		Puint64: 1,
		Pbool:   true,
		Pstring: "some",
	}

	for i := 0; i < numberOfRuns; i++ {
		localStartTime := time.Now()

		for j := 0; j < numberOfrequestsByRun; j++ {
			res, err := http.Get(fmt.Sprintf("http://localhost:7000/get-data?%s", data.ToGetParams()))
			if err != nil {
				panic(err)
			}

			// dataJSON, err := json.Marshal(data)
			// if err != nil {
			// 	panic(err)
			// }

			// res, err := http.Post(
			// 	"http://localhost:7000/post-data",
			// 	"application/json",
			// 	bytes.NewBuffer(dataJSON),
			// )
			// if err != nil {
			// 	panic(err)
			// }

			res.Body.Close()
		}

		fmt.Printf("Local time for %d run: %v\n", i, time.Since(localStartTime))
	}
}

func rungRPCTest() {
	ctx := context.Background()

	conn, err := grpc.NewClient(
		"localhost:7002",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	client := pb.NewGrpcRestTestServiceClient(conn)

	for i := 0; i < numberOfRuns; i++ {
		localStartTime := time.Now()

		for j := 0; j < numberOfrequestsByRun; j++ {
			_, err := client.PostData(ctx, &pb.PostDataRequest{
				Pdouble: 1.1,
				Pfloat:  1.1,
				Pint32:  -1,
				Pint64:  -1,
				Puint32: 1,
				Puint64: 1,
				Pbool:   true,
				Pstring: "some",
			})
			if err != nil {
				panic(err)
			}
		}

		fmt.Printf("Local time for %d run: %v\n", i, time.Since(localStartTime))
	}
}
